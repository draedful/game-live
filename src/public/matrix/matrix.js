import MatrixInterface from './matrix_interface.js';


export default class ArrayMatrix extends MatrixInterface {
  
  constructor(width, height) {
    super();
    this.matrix = new Array(height).map(() => new Int8Array(width));
    this.newMatrix = new Array(height).map(() => new Int8Array(width));
  }
  
  get(rowIndex, cellIndex) {
    return this.matrix[rowIndex][cellIndex];
  }
  
  set(rowIndex, cellIndex, value) {
    this.matrix[rowIndex][cellIndex] = +(!!value); // hard convert value to 0 or 1
    return this;
  }
  
  map(cb) {
    for ( let rowIndex = 0; rowIndex < this.matrix.length; rowIndex++ ) {
      for ( let cellIndex = 0; cellIndex < this.matrix[rowIndex].length; cellIndex++ ) {
        this.newMatrix[rowIndex][cellIndex] = cb(this.get(rowIndex, cellIndex), rowIndex, cellIndex, this);
      }
    }
  }
  
}