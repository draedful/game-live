export default class MatrixInterface {
  
  
  get(rowIndex, cellIndex) {
    
  }
  
  set(rowIndex, cellIndex) {
  
  }
  
  left(rowIndex, cellIndex) {
    return this.get(rowIndex, cellIndex - 1);
  }
  
  right(rowIndex, cellIndex) {
    return this.get(rowIndex, cellIndex + 1);
  }
  
  top(rowIndex, cellIndex) {
    return this.get(rowIndex - 1, cellIndex);
  }
  
  bottom(rowIndex, cellIndex) {
    return this.get(rowIndex + 1, cellIndex);
  }
  
  topLeft(rowIndex, cellIndex) {
    return this.get(rowIndex - 1, cellIndex - 1);
  }
  
  topRight(rowIndex, cellIndex) {
    return this.get(rowIndex - 1, cellIndex + 1);
  }
  
  bottomLeft(rowIndex, cellIndex) {
    return this.get(rowIndex + 1, cellIndex - 1);
  }
  
  bottomRight(rowIndex, cellIndex) {
    return this.get(rowIndex + 1, cellIndex + 1);
  }
  
}