import MatrixInterface from './matrix_interface.js';


export default class FlatMatrix extends MatrixInterface {
  
  constructor(width, height) {
    super();
    this.width = width;
    this.height = height;
    
    this.matrix = new Int8Array(width * height);
    this.newMatrix = new Int8Array(width * height);
  }
  
  getRowIndex(index) {
    return Math.floor(index / this.height);
  }
  
  isLeftEdge(index) {
    return this.getRowIndex(index) > this.getRowIndex(index - 1);
  }
  
  isRightEdge(index) {
    return this.getRowIndex(index) < this.getRowIndex(index + 1);
  }
  
  isTopEdge(index) {
    return index < this.width
  }
  
  isBottomEdge(index) {
    return index > ((this.height - 1) * this.width);
  }
  
  hasRow(rowIndex) {
    return this.height <= rowIndex;
  }
  
  left(index) {
    if ( !this.isLeftEdge(index) ) {
      return this.matrix[index - 1];
    }
    return null;
  }
  
  right(index) {
    if ( !this.isRightEdge(index) ) {
      return this.matrix[index + 1];
    }
    return null;
  }
  
  top(index) {
    if ( !this.isTopEdge(index) ) {
      return this.matrix[index - this.width];
    }
    return null;
  }
  
  bottom(index) {
    if ( !this.isBottomEdge(index) ) {
      return this.matrix[index + this.width];
    }
    return null;
  }
  
  topRight(index) {
    if ( !this.isTopEdge(index) && !this.isRightEdge(index) ) {
      return this.matrix[index - this.width + 1];
    }
    return null;
  }
  
  topLeft(index) {
    if ( !this.isTopEdge(index) && !this.isLeftEdge(index) ) {
      return this.matrix[index - this.width - 1];
    }
    return null;
  }
  
  bottomLeft(index) {
    if ( !this.isBottomEdge(index) && !this.isLeftEdge(index) ) {
      return this.matrix[index + this.width - 1];
    }
    return null;
  }
  
  bottomRight(index) {
    if ( !this.isBottomEdge(index) && !this.isRightEdge(index) ) {
      return this.matrix[index + this.width + 1];
    }
    return null;
  }
  
  getFlatIndex(rowIndex, cellIndex) {
    return (this.height * rowIndex) + cellIndex;
  }
  
  getTwoDimencionalCoords(index) {
    const row = parseInt(index / this.height);
    const cell = parseInt(index % this.width);
    return [row, cell];
  }
  
  get(rowIndex, cellIndex) {
    return this.matrix[this.getFlatIndex(rowIndex, cellIndex)];
  }
  
  set(rowIndex, cellIndex, value) {
    this.matrix[this.getFlatIndex(rowIndex, cellIndex)] = +(!!value);
    return this;
  }
  
  map(cb) {
    for ( let index = 0; index < this.matrix.length; index++ ) {
      this.newMatrix[index] = cb(this.matrix[index], index, this);
    }
    this.matrix = this.newMatrix;
  }
  
}