import FlatMatrix from './flat_matrix.js';
import Matrix from './matrix.js';

export const createFlatMatrix = (width, height) => new FlatMatrix(width, height);
export const createMatrix = (width, height) => new Matrix(width, height);