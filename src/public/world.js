import FlatMatrix from './matrix/flat_matrix.js';

const count = (...args) => args.filter((item) => item).length;


export default class World extends FlatMatrix {
  
  mustDie(index) {
    const countLive = count(
      this.left(index),// 1
      this.right(index), // 2
      this.top(index), // 3
      this.topRight(index), //4
      this.topLeft(index), // 5
      this.bottom(index), // 6
      this.bottomRight(index), // 7
      this.bottomLeft(index)
    );
    return countLive < 2 || countLive > 3;
  }
  
  mustAlive(index) {
    const countL = count(
      this.left(index),// 1
      this.right(index), // 2
      this.top(index), // 3
      this.topRight(index), //4
      this.topLeft(index), // 5
      this.bottom(index), // 6
      this.bottomRight(index), // 7
      this.bottomLeft(index)
    );
    return countL === 3;
  }
  
  step(cb) {
    console.time('step');
    this.map((live, index) => {
      let newValue = live;
      if ( live ) {
        if ( this.mustDie(index) ) {
          newValue = 0;
        }
      } else if ( this.mustAlive(index) ) {
        newValue = 1;
      }
      if ( newValue !== live ) {
        const [rowIndex, cellIndex] = this.getTwoDimencionalCoords(index);
        cb(rowIndex, cellIndex, newValue);
      }
      return newValue;
    });
    console.timeEnd('step');
    return this;
  }
  
  
}
