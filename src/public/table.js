const getTableMarkup = (rows, cells) => new Array(rows).fill('').map(() => `<tr>${'<td></td>'.repeat(cells)}</tr>`).join('');

export default class TableRenderer {
  
  constructor(table, width, height) {
    this.table = table;
    this.table.innerHTML = getTableMarkup(height, width)
  }
  
  hasRow(rowIndex) {
    return this.table.rows.length > rowIndex;
  }
  
  hasCell(rowIndex, cellIndex) {
    return this.hasRow(rowIndex) && this.table.rows[rowIndex].cells.length > cellIndex;
  }
  
  getCell(rowIndex, cellIndex) {
    return this.hasCell(rowIndex, cellIndex) ? this.table.rows[rowIndex].cells[cellIndex] : null;
  }
  
  updateCell(rowIndex, cellIndex, cb) {
    cb(this.getCell(rowIndex, cellIndex));
    return this;
  }
  
}

