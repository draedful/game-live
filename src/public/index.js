import World from './world.js';
import Table from './table.js';

const WorldHeight = 100;
const WorldWidth = 100;

window.world = new World(WorldWidth, WorldHeight);

const table = document.getElementById('table');

const tableRenderer = new Table(table, WorldWidth, WorldHeight);

const updateCell = (cell, value) => cell.classList.toggle('live', !!value);

let stopWorld = false;

const start = () => requestAnimationFrame(animationStep);
const stop = () => stopWorld = true;
const step = () => world.step((rowIndex, cellIndex, live) => updateCell(tableRenderer.getCell(rowIndex, cellIndex), live));

function animationStep() {
  if ( !stopWorld ) {
    start();
    stopWorld = false
  }
  step();
}

table.onclick = function (e) {
  const elem = e.target;
  const parent = e.target.parentNode;
  
  const cellIndex = elem.cellIndex;
  const rowIndex = parent.rowIndex;
  const value = !world.get(rowIndex, cellIndex);
  world.set(rowIndex, cellIndex, value);
  updateCell(tableRenderer.getCell(rowIndex, cellIndex), value);
};

document.getElementById('start').onclick = start;
document.getElementById('stop').onclick = stop;
document.getElementById('step').onclick = step;